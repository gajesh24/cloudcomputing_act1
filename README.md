# CloudComputing_Act1

## Task description
The goal of this exercise is to have a “TODO app” that is running locally on your machine (not as a Docker container) and has a front-end that can be viewed in the browser. Requirements:
- A user can add new TODO items to a list (e.g., “I need to buy groceries tomorrow”).
- A user can view the full list of TODOs.
- A user can mark a TODO as “done”; this will hide the entry.
- A user can delete a TODO, this will remove the entry.
- A user can do all of the tasks above in a browser-based User Interface (web application).
- The TODO list needs to be stored in an external database.
- The app and database need to run locally, don’t use online services for hosting the app or database (if you know how, you can run a local docker container for the database only)
- Users can register a username/password
- Users can then log in with their username/password
- Each User can only view/manage their own TODO list and can’t access the TODO
lists of other users
- User accounts need to be stored securely in the database (password salted and hashed
e.g., using BCRYPT)

Develop a TODO app that fulfills the requirements from above. You can use any language
and framework that you want. The app needs to be connected to a database to store the TODO
lists. Use MongoDB as the database.
---
## Prerequisites
- Git
- Docker, Docker Compose
- Node JS, NPM

---
## Deployment
In exercise 1, the deployment consisted of 2 parts: deploying the database and the application. However, in exercise 2, the application got containerized, so you can deploy it by following these simple steps:
1. Clone the repository:
`git clone https://gitlab.reutlingen-university.de/gajesh24/cloudcomputing_act1.git`
2. Enter the folder:
`cd cloudcomputing_act1`
3. Create .env file with the following contents:
`
MONGO_INITDB_ROOT_USERNAME=mongoAdmin
MONGO_INITDB_ROOT_PASSWORD=someRandomPassword123
MONGO_URI=mongodb://$MONGO_INITDB_ROOT_USERNAME:$MONGO_INITDB_ROOT_PASSWORD@mongodb:27017/todo-app?authSource=admin
`
4. Build the application
`sudo docker compose build`
5. Run the application
`sudo docker compose --env-file .env up -d`
6. Open up website on `http://localhost/`
7. To stop the application
`sudo docker compose stop`

---
## Authors
Created by Jesus Galaz Reyes and Rokas Stankūnas.

---
## License
This project is licensed under the ISC license.