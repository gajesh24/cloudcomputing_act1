# We're taking the latest Alpine version, because it's the smallest
# and has the least amount of dependencies
FROM alpine:latest

# Set working directory.
WORKDIR /app

# Copy the current directory contents (whole project) into the container at /app
ADD . /app

# Install dependencies
RUN apk update && \
    apk add --no-cache nodejs npm && \
    npm install;

# Run the app
ENTRYPOINT ["node", "server"]