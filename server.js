const express = require('express');
const path = require('path');
const connectDB = require('./mongodb');
const session = require('express-session');

const app = express();

// Middleware
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

// Session configuration
app.use(
  session({
    secret: 'session_secret',
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false }
  })
);

// Connection to MongoDB
connectDB();

// Routes
app.use('/api/todos', require('./routes/todos'));
app.use('/api/users', require('./routes/users'));

// Initialize the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
