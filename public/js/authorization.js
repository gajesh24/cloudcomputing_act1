document.addEventListener('DOMContentLoaded', async () => {
    // Event for checking if the user is logged in
    try {
      const res = await fetch('/api/users/loggedin', { method: 'POST' });
      if (!res.ok) {
        window.location.href = 'login.html';  
      }
    } catch (err) {
      console.error('Error while checking if a user is logged in:', err);
    }
});