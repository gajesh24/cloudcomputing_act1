document.addEventListener('DOMContentLoaded', async () => {
  const form = document.getElementById("new-task-form");
  const input = form.querySelector('input[name="task"]');
  const taskList = document.getElementById("task-list");
  const logoutBtn = document.getElementById('logout-btn'); 
  const toggleCompletedBtn = document.getElementById('toggle-completed-btn'); 

  let showCompletedTasks = false;  

  // Function to check if the currently processed task belongs to user account
  async function isValidTask(taskId) {
    try {
      const res = await fetch("/api/todos"); 
      const todos = await res.json();
      let valid = false;

      todos.forEach(todo => {
        if (todo._id == taskId)
          valid = true;
      });
      return valid;
    } catch (err) {
      console.error("Error loading tasks:", err);
    }
  }

  // Load the tasks when page is initiated
  async function loadTasks() {
    try {
      const res = await fetch("/api/todos"); 
      const todos = await res.json(); 

      // Verify if the response has tasks
      if (todos.length > 0) {
        renderTasks(todos); 
      } else {
        console.log('No tasks to show.');
      }
    } catch (err) {
      console.error("Error loading tasks:", err);
    }
  }

  // Function to render tasks in the list
  function renderTasks(todos) {
    taskList.innerHTML = '';  
    todos.forEach(todo => {
      const taskItem = createTaskElement(todo);  
      taskList.appendChild(taskItem); 
    });

    toggleCompletedVisibility(); 
  }

  // Function to create an element in DOM
  function createTaskElement(todo) {
    const li = document.createElement('li'); 
    li.dataset.id = todo._id; 
    if (todo.isDone) {
      li.classList.add('completed');  
      li.style.display = 'none'; 
    }

    li.innerHTML = `
      <span class="task-text">${todo.description}</span>
      <input type="text" class="edit-input" value="${todo.description}" style="display:none;">
      <div class="task-actions">
        <button class="complete-btn" title="Mark as complete">✓</button>
        <button class="edit-btn" title="Edit task">✎</button>
        <button class="save-btn" title="Save changes" style="display:none;">💾</button>
        <button class="delete-btn" title="Delete task">×</button>
      </div>
    `;

    // Button assigns
    const completeBtn = li.querySelector(".complete-btn");
    completeBtn.addEventListener("click", async () => {
      await toggleComplete(todo._id, !todo.isDone);
      li.classList.toggle("completed");  
      toggleCompletedVisibility(); 
    });

    const deleteBtn = li.querySelector(".delete-btn");
    deleteBtn.addEventListener("click", async () => {
      await deleteTask(todo._id);
      li.remove(); 
    });

    const editBtn = li.querySelector(".edit-btn");
    const saveBtn = li.querySelector(".save-btn");
    const taskText = li.querySelector(".task-text");
    const editInput = li.querySelector(".edit-input");

    editBtn.addEventListener("click", () => {
      taskText.style.display = "none";  
      editInput.style.display = "block";  
      editInput.focus();
      saveBtn.style.display = "inline"
      editBtn.style.display = "none";  
    });

    saveBtn.addEventListener("click", async () => {
      const newDescription = editInput.value.trim();
      if (newDescription && newDescription !== taskText.textContent) {
        await updateTask(todo._id, newDescription);
        taskText.textContent = newDescription;
      }
      taskText.style.display = "block";  
      editInput.style.display = "none";  
      saveBtn.style.display = "none"; 
      editBtn.style.display = "inline";  
    });

    return li;  
  }

  // Function to toggle completed task visibility
  function toggleCompletedVisibility() {
    const completedTasks = document.querySelectorAll('.completed');
    completedTasks.forEach(task => {
      if (showCompletedTasks) {
        task.style.display = 'block';
      } else {
        task.style.display = 'none';
      }
    });
  }

  // Toggle button event listener
  toggleCompletedBtn.addEventListener('click', () => {
    showCompletedTasks = !showCompletedTasks;
    toggleCompletedVisibility();
    toggleCompletedBtn.textContent = showCompletedTasks ? 'Hide Completed Tasks' : 'Show Completed Tasks';
  });

  // Function to alternate to the completed state
  async function toggleComplete(taskId, isDone) {
    // Input sanitation 
    if (isNaN(parseInt(taskId, 10)) && typeof(isDone) === 'boolean' && isValidTask(taskId)) {
      console.error('Invalid task ID, task status is invalid or task does not belong to account');
      return;
    }

    try {
        const res = await fetch(`/api/todos/${taskId}/done`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ isDone })
      });
      if (!res.ok) {
        console.error('Error marking task as complete.');
      }
    } catch (err) {
      console.error('Error updating task:', err);
    }
  }

  // Function to eliminate a task
  async function deleteTask(taskId) {
    // Input sanitation 
    if (isNaN(parseInt(taskId, 10))) {
      console.error('Invalid task ID');
      return;
    }
    
    try {
      const res = await fetch(`/api/todos/${taskId}`, {
        method: 'DELETE'
      });
      if (!res.ok) {
        console.error('Error deleting task.');
      }
    } catch (err) {
      console.error('Error deleting task:', err);
    }
  }

  // Function to update the description of a task
  async function updateTask(taskId, newDescription) {
    // Input sanitation 
    if (isNaN(parseInt(taskId, 10)) && typeof(newDescription) === 'string' && isValidTask(taskId)) {
      console.error('Invalid task ID, task status is invalid or task does not belong to account');
      return;
    }
  
    try {
      const res = await fetch(`/api/todos/${taskId}/description`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ description: newDescription })
      });
      if (!res.ok) {
        console.error('Error updating task.');
      }
    } catch (err) {
      console.error('Error updating task:', err);
    }
  }

  // Load the tasks in the beginning
  loadTasks();
  
  // Add new task
  form.addEventListener("submit", async (e) => {
    e.preventDefault();
    const description = input.value.trim();
    if (description) {
      await addTask(description);
      input.value = ""; 
    }
  });

  // Function to add new task
  async function addTask(description) {
    try {
      const res = await fetch("/api/todos", {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ description })
      });
      if (res.ok) {
        const newTodo = await res.json();
        taskList.appendChild(createTaskElement(newTodo));  
      } else {
        console.error('Error adding task.');
      }
    } catch (err) {
      console.error('Error adding task:', err);
    }
  }

  // Logout button event
  logoutBtn.addEventListener('click', async () => {
      try {
        const res = await fetch('/api/users/logout', { method: 'POST' });
        if (res.ok) {
          window.location.href = 'login.html';  
        } else {
          console.error('Logout failed');
        }
      } catch (err) {
        console.error('Error during logout:', err);
      }
  });
});
