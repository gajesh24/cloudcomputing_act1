document.addEventListener('DOMContentLoaded', () => {
  
  // Register
  const registerForm = document.getElementById('register-form');
  if (registerForm) {
    registerForm.addEventListener('submit', async (event) => {
      event.preventDefault(); 

      const username = document.getElementById('username').value;
      const password = document.getElementById('password').value;

      try {
        const response = await fetch('/api/users/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ username, password }),
        });

        const result = await response.json();

        if (response.ok) {
          document.querySelector('.success_message').style.display = 'block';
          document.querySelector('.error_text').textContent = '';
          setTimeout(() => {
            window.location.href = 'login.html';
          }, 2000); 
        } else {
          document.querySelector('.error_text').textContent = result.error || 'Registration failed';
          document.querySelector('.success_message').style.display = 'none';
        }
      } catch (error) {
        console.error('Error:', error);
        document.querySelector('.error_text').textContent = 'Server error. Please try again later.';
      }
    });
  }

  // Login In
  const loginForm = document.getElementById('login-form');
  if (loginForm) {
    loginForm.addEventListener('submit', async (event) => {
      event.preventDefault(); 

      const username = document.getElementById('username').value;
      const password = document.getElementById('password').value;

      try {
        const response = await fetch('/api/users/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ username, password }),
        });

        const result = await response.json();

        if (response.ok) {
          document.querySelector('.success_message').style.display = 'block';
          document.querySelector('.error_text').textContent = '';
          setTimeout(() => {
            window.location.href = 'index.html';
          }, 2000);
        } else {
          document.querySelector('.error_text').textContent = result.error || 'Login failed';
          document.querySelector('.success_message').style.display = 'none';
        }
      } catch (error) {
        console.error('Error:', error);
        document.querySelector('.error_text').textContent = 'Server error. Please try again later.';
      }
    });
  }
});