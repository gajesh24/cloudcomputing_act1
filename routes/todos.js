const express = require('express');
const router = express.Router();
const Todo = require('../models/todo');

// Middleware to verify if the user is authenticated
function isAuthenticated(req, res, next) {
  if (req.session.user) {
    return next();
  } else {
    return res.status(401).json({ error: 'Unauthorized' });
  }
}

// Route to logout
router.post('/logout', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      console.error('Error al destruir la sesión:', err);
      return res.status(500).json({ error: 'Error al cerrar sesión' });
    }
    res.status(200).json({ message: 'Logout successful' });
  });
});

// Route to obtain all tasks of authenticated user
router.get('/', isAuthenticated, async (req, res) => {
  try {
    const todos = await Todo.find({ userId: req.session.user.id });
    res.json(todos);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
});

// Route to add a task
router.post('/', isAuthenticated, async (req, res) => {
  const { description } = req.body;
  
  try {
    const todo = new Todo({ description, userId: req.session.user.id });
    await todo.save();
    res.json(todo);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
});


// Route to eliminate tasks
router.delete('/:id', isAuthenticated, async (req, res) => {
  try {
    const todo = await Todo.findById(req.params.id);

    if (!todo || todo.userId.toString() !== req.session.user.id) {
      return res.status(404).json({ error: 'Todo not found or unauthorized' });
    }

    await todo.deleteOne(); 
    res.json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
});

// Route to update the description of a task
router.put('/:id/description', isAuthenticated, async (req, res) => {
  const { description } = req.body;
  
  try {
    const todo = await Todo.findById(req.params.id);
    
    if (!todo || todo.userId.toString() !== req.session.user.id) {
      return res.status(404).json({ error: 'Todo not found or unauthorized' });
    }

    todo.description = description;
    await todo.save();

    res.json({ todo });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
});

// Route to mark a task as completed
router.put('/:id/done', isAuthenticated, async (req, res) => {
  const { isDone } = req.body;
  
  try {
    const todo = await Todo.findById(req.params.id);

    if (!todo || todo.userId.toString() !== req.session.user.id) {
      return res.status(404).json({ error: 'Todo not found or unauthorized' });
    }

    todo.isDone = isDone;
    await todo.save();

    res.json({ todo });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
});

module.exports = router;