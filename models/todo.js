const mongoose = require('mongoose');

const TodoSchema = new mongoose.Schema({
  description: { type: String, required: true },
  isDone: { type: Boolean, default: false },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
});

module.exports = mongoose.model('Todo', TodoSchema);
